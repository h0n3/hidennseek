//
// Created by h on 06.02.20.
//
#include <iostream>

#include <boost/dll.hpp>

#include "segvcatch.h"
#include "types.hpp"

char* alloc_easy_structure()
{
    const size_t length = 100;
    auto         result = new char[length];

    for (size_t index = 0; index < length; index++) {
        result[index] = 'X';
    }

    return result;
};

#include <stdexcept>

static search_result_t test_for_test_child(const test_child* tc)
{
    try {
        const char* str = tc->const_chr_ptr;
        std::size_t len = tc->length;


        if (str == nullptr) {
            return (len) ? miss : hit;
        } else {
            return (str[len] == '0') ? hit : miss;
        }
    } catch (std::runtime_error& ex) {
        std::cout << "s: " << std::endl;
    }
}

static search_result_t test_for_test_structure(const test_structure* ts)
{
    if (ts->child1) {
        auto result = test_for_test_child(ts->child1);
        if (result != ambiguous)
            return result;
    }

    if (ts->child2) {
        auto result = test_for_test_child(ts->child2);
        if (result != ambiguous)
            return result;
    }

    return ambiguous;
}

test_structure* seek_impl(void* hint)
{
    char* start_addr = reinterpret_cast<char*>(hint);

    for (char* addr = start_addr; true; addr++) {
        try {

            auto ts  = reinterpret_cast<const test_structure*>(addr);
            auto res = test_for_test_structure(ts);

            switch (res) {
            case hit: {
                std::cout << "Found hit at: " << addr << std::endl;
                return nullptr;
            }
            case ambiguous: {
                std::cout << "a: " << addr << std::endl;
            }
            }
        } catch (std::runtime_error& ex) {
            std::cout << "s: " << addr << std::endl;
            continue;
        }
    }
}

int main(int argc, char** argv)
{
    // Simulate the event
    segvcatch::init_segv();
    /*
        boost::dll::shared_library shared_lib{argv[1]};
        auto seek_func = shared_lib.get<seek_func_t>("seek_func");
    */
    char* hint = alloc_easy_structure();

    const size_t padding_elements = 150;
    for (size_t padding_element = 0; padding_element < padding_elements; padding_element++) {
        alloc_easy_structure();
    }

    auto target = make_valid_test_structure("Hist", "derboss");

    auto result = seek_impl(hint);

    if (target == result) {
        std::cout << "skrrt" << std::endl;
    }
}