//
// Created by h on 06.02.20.
//

#ifndef HIDENSEEK_SEEK_HPP
#define HIDENSEEK_SEEK_HPP

#include <iostream>
#include <cstring>

#include "types.hpp"

#include <boost/dll.hpp>

test_structure* seek_impl(void *hint);

BOOST_DLL_ALIAS(seek_impl, seek_func);

#endif //HIDENSEEK_SEEK_HPP
