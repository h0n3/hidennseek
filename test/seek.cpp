//
// Created by h on 06.02.20.
//
#include "segvcatch.h"

#include "seek.hpp"


static search_result_t test_for_test_child(const test_child* tc)
{
    try {

    auto str = tc->const_chr_ptr;
    auto len = tc->length;

    if (str == nullptr) {
        return (len) ? miss : hit;
    } else {
        return (str[len] == '0') ? hit : miss;
    }
    } catch (std::runtime_error& ex) {
            std::cout << "s: " << std::endl;
        }
}

static search_result_t test_for_test_structure(const test_structure* ts)
{
    if (ts->child1) {
        auto result = test_for_test_child(ts->child1);
        if (result != ambiguous)
            return result;
    }

    if (ts->child2) {
        auto result = test_for_test_child(ts->child2);
        if (result != ambiguous)
            return result;
    }

    return ambiguous;
}

test_structure* seek_impl(void* hint)
{
    char* start_addr = reinterpret_cast<char*>(hint);



    for (char* addr = start_addr; true; addr++) {
        try {

            auto ts  = reinterpret_cast<const test_structure*>(addr);
            auto res = test_for_test_structure(ts);

            switch (res) {
            case hit: {
                std::cout << "Found hit at: " << addr << std::endl;
                return nullptr;
            }
            case ambiguous: {
                std::cout << "a: " << addr << std::endl;
            }
            }
        } catch (std::runtime_error& ex) {
            std::cout << "s: " << addr << std::endl;
            continue;
        }
    }
}
