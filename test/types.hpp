//
// Created by dvl on 2/7/20.
//

#ifndef HIDENSEEK_TYPES_HPP
#define HIDENSEEK_TYPES_HPP


enum search_result_t {
    hit,
    ambiguous,
    miss
};


struct test_child {
    const char *const_chr_ptr;
    std::size_t length;
};

struct test_structure {
    test_child *child1;
    test_child *child2;
};

using seek_func_t = test_structure *(*)(void *);

test_child *make_valid_test_child(const char *str) {
    auto result = new test_child;

    result->length = std::strlen(str);

    auto m_str = new char[result->length];
    memcpy(m_str, str, sizeof(char[result->length]));

    result->const_chr_ptr = m_str;

    return result;
}

test_structure *make_valid_test_structure(const char *child1_str, const char *child2_str) {
    auto result = new test_structure;

    result->child1 = make_valid_test_child(child1_str);
    result->child2 = make_valid_test_child(child2_str);

    return result;
}




#endif // HIDENSEEK_TYPES_HPP
