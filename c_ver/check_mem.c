//
// Created by dvl on 2/10/20.
//

#include "check_mem.h"

#include <stdlib.h>

#include <signal.h>
#include <setjmp.h>



static jmp_buf next_elem_buf;

static void sigsev_handler(int sig, siginfo_t* si, void* unused)
{
    longjmp(next_elem_buf, 1);
}

int init_mem_check(){

    struct sigaction sa;

    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = sigsev_handler;
    return sigaction(SIGSEGV, &sa, NULL) == -1;
}


int try_to_access(heap_location_t* heap_location, uintptr_t addr)
{
    if (!is_ptr_in_heap(heap_location, addr)) {
        return -1;
    }

    if (setjmp(next_elem_buf)) {
        return -2;
    }

    char* ptr   = (char*)addr;
    char  value = *ptr;

    return 1;
}

int try_to_access_n(heap_location_t* heap_location, uintptr_t addr, uintptr_t n)
{
    for (uintptr_t offset = 0; offset < n; offset++) {
        int result = try_to_access(heap_location, addr + offset);
        if (result < 0) {
            return result;
        }
    }
    return 1;
}

void* check_mem_loc_ptr(heap_location_t* heap_location, uintptr_t addr)
{
    const size_t length = sizeof(void*);

    int result = try_to_access_n(heap_location, addr, length);
    if (result < 0) {
        return NULL;
    }

    return (void*)addr;
}
