//
// Created by h on 07.02.20.
//

#ifndef HIDENSEEK_HEAP_FIND_H
#define HIDENSEEK_HEAP_FIND_H

#include <stdint.h>

typedef struct heap_location_s {
    uintptr_t start;
    uintptr_t end;
} heap_location_t;

typedef struct heap_find_s {
    char* maps_location;
} heap_find_t;

void init_heap_find(heap_find_t* heap_find);
void free_heap_find(heap_find_t* heap_find);

heap_location_t find_heap(heap_find_t* heap_find);

int is_ptr_in_heap(heap_location_t *heap_location, uintptr_t addr);
void print_heap_location(heap_location_t* heap_location);

#endif //HIDENSEEK_HEAP_FIND_H
