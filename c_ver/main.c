//
// Created by dvl on 2/7/20.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>


#include "heap_find.h"
#include "check_mem.h"

typedef enum search_result_e { hit, ambiguous, miss } search_result_t;

typedef struct test_child_s {
    const char* const_chr_ptr;
    size_t      length;
} test_child_t;

typedef struct test_structure_s {
    test_child_t* child1;
    test_child_t* child2;
} test_structure_t;

test_child_t* make_valid_test_child(const char* str)
{
    test_child_t* result = (test_child_t*)malloc(sizeof(test_child_t));

    result->length = strlen(str);

    const size_t size  = sizeof(char) * result->length + 1;
    char*        m_str = (char*)malloc(size);
    memcpy(m_str, str, size);

    result->const_chr_ptr = m_str;

    return result;
}

test_structure_t* make_valid_test_structure(const char* child1_str, const char* child2_str)
{
    test_structure_t* result = (test_structure_t*)malloc(sizeof(test_structure_t));

    result->child1 = make_valid_test_child(child1_str);
    result->child2 = make_valid_test_child(child2_str);

    return result;
}


DEFINE_CHECK_MEM_LOC_TYPE(test_structure_t)
DEFINE_CHECK_MEM_LOC_TYPE(test_child_t)

static search_result_t test_for_test_child(heap_location_t* heap_location, const test_child_t* tc)
{
    uintptr_t str_addr = (uintptr_t)tc->const_chr_ptr;

    const char* str = check_mem_loc_ptr(heap_location, str_addr);
    size_t      len = tc->length;

    if (str == NULL) {
        return (len) ? miss : hit;
    } else {
        if (!is_ptr_in_heap(heap_location, (uintptr_t)str))
            return miss;

        char delim = str[len];
        return (delim) ? miss : hit;
    }
}

static search_result_t test_for_test_structure(
    heap_location_t* heap_location, const test_structure_t* ts)
{

    if (ts->child1) {
        uintptr_t     child1_addr = (uintptr_t)ts->child1;
        test_child_t* test_child1 = check_mem_loc_test_child_t(heap_location, child1_addr);
        if (test_child1 == NULL) {
            return miss;
        }

        search_result_t result = test_for_test_child(heap_location, test_child1);
        if (result != hit)
            return result;
    } else {
        return miss;
    }
    if (ts->child2) {
        uintptr_t     child2_addr = (uintptr_t)ts->child2;
        test_child_t* test_child2 = check_mem_loc_test_child_t(heap_location, child2_addr);
        if (test_child2 == NULL) {
            return miss;
        }

        search_result_t result = test_for_test_child(heap_location, test_child2);
        if (result != hit)
            return result;
    } else {
        return miss;
    }
    return hit;
}

test_structure_t* seek_obj(heap_location_t* heap_location)
{
    for (uintptr_t addr = heap_location->start; addr < heap_location->end; addr++) {

        // printf("Inspection for mem region: %lx \t", addr); //\n

        const size_t size_test_struc = sizeof(test_structure_t*);
        size_t       offset;
        /*
                for (offset = 0; offset < size_test_struc; offset++) {
                    printf("%x|", addr);
                }
        */
        test_structure_t* test_struc = check_mem_loc_test_structure_t(heap_location, addr);

        if (test_struc) {
            search_result_t result = test_for_test_structure(heap_location, test_struc);
            if (result == hit) {
                return test_struc;
            }
        }

        // printf("\n");
    }
}

char* alloc_easy_structure()
{
    const size_t size = 500 * sizeof(char);

    char* result = (char*)malloc(size);
    memset(result, 8, size);

    return result;
}

int main(int argc, char** argv)
{
    heap_find_t heap_find;
    init_heap_find(&heap_find);

    const size_t padding_elements = 150;
    size_t       padding_element;
    for (padding_element = 0; padding_element < padding_elements; padding_element++) {
        alloc_easy_structure();
    }

    test_structure_t* target = make_valid_test_structure("Hist", "derboss");
    printf("Created target at location: 0x%lx\n", (uintptr_t)target);

    heap_location_t heap_loc = find_heap(&heap_find);
    print_heap_location(&heap_loc);

    init_mem_check();

    test_structure_t* result = seek_obj(&heap_loc);

    if (target == result) {
        printf("Found target");
    } else {
        printf("Couldnt find target");
    }
}