//
// Created by h on 07.02.20.
//
#include "heap_find.h"

#include <unistd.h>
#include <sys/types.h>


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_heap_find(heap_find_t* heap_find){
    pid_t pid = getpid();

    const size_t maps_loc_str_size = 30;
    char* maps_loc_str = (char*) malloc(sizeof(*maps_loc_str) * maps_loc_str_size);
    snprintf(maps_loc_str, maps_loc_str_size, "/proc/%i/maps", pid);

    heap_find->maps_location = maps_loc_str;
}

void free_heap_find(heap_find_t* heap_find){
    free(heap_find->maps_location);
}

static int is_heap_line(char* line){
    return strstr(line, "[heap]") != NULL;
}

static  heap_location_t parse_heap_line(char* line){
      heap_location_t result;

      char* end = strchr(line, '-');
      *end = '\0';

      char* parse_end = NULL;
      result.start = strtoll(line, &parse_end, 16);

      if (parse_end != end){
          //sth is wrong here
      }

      line = end + 1;

      end = strchr(line, ' ');
      *end = '\0';

      parse_end = NULL;
      result.end =  strtoll(line, &parse_end, 16);

      if (parse_end != end){
          //sth is wrong here
      }

      return result;
}

heap_location_t find_heap(heap_find_t* heap_find){

    FILE* maps_file = fopen(heap_find->maps_location, "r");

    char* line = NULL;
    size_t  line_size = 0;

    do {
        getdelim(&line, &line_size, '\n', maps_file);

        if (line_size && is_heap_line(line)){
            return parse_heap_line(line);
        } else {
            continue;
        }

    } while (line_size);

}

int is_ptr_in_heap(heap_location_t *heap_location, uintptr_t addr) {
    if (addr < heap_location->start)
        return 0;

    if (addr > heap_location->end)
        return 0;

    return 1;
}

void print_heap_location(heap_location_t* heap_location){
    printf("Heap location: 0x%lx-0x%lx\n", heap_location->start, heap_location->end);
}