//
// Created by dvl on 2/10/20.
//

#ifndef HIDENSEEK_CHECK_MEM_H
#define HIDENSEEK_CHECK_MEM_H

#include "heap_find.h"

int init_mem_check();

int try_to_access(heap_location_t* heap_location, uintptr_t addr);
int try_to_access_n(heap_location_t* heap_location, uintptr_t addr, uintptr_t n);


void* check_mem_loc_ptr(heap_location_t* heap_location, uintptr_t addr);

#define DEFINE_CHECK_MEM_LOC_TYPE(TYPE)                                        \
    TYPE* check_mem_loc_##TYPE(heap_location_t* heap_location, uintptr_t addr) \
    {                                                                          \
        const size_t length = sizeof(TYPE);                                    \
        int          result = try_to_access_n(heap_location, addr, length);    \
        if (result < 0) {                                                      \
            return NULL;                                                       \
        }                                                                      \
        return (void*)addr;                                                    \
    }



#endif // HIDENSEEK_CHECK_MEM_H
