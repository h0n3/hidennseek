//
// Created by h on 01.02.20.
//

#ifndef HIDENSEEK_STRUCTURE_HPP
#define HIDENSEEK_STRUCTURE_HPP

#include <cstddef>
#include <vector>
#include <memory>

#include "types.hpp"


namespace structure {

    class interface {
    public:
        /**
         * Returns the size of the represented memory block in bytes
         * @return
         */
        virtual std::size_t get_size() = 0;
        virtual bool  is_writeable() = 0;

        /**
         * \brief This function is supposed to return a likelihood score rating how likely it is that the memory region pointed to by mem belongs to the given structure
         * @param mem Pointer to the actual memory
         * @param expected_buffer_len The remaining size of the memory block after the address that mem points to.
         */
        virtual likelihood analyse_mem(void *mem) = 0;
    };

    template<class value_t>
    class pointer : interface {
        using ptr_t = value_t *;

        virtual std::size_t size() {
            return sizeof(ptr_t);
        }

        virtual likelihood analyse_mem(void *mem) {
            if (mem == nullptr) {
                return neutral;
            }
            auto ptr = reinterpret_cast<ptr_t>(mem);
            return value_t::is_valid(*ptr);
        }
    };

    class compound : public interface {
        using ptr_t = std::unique_ptr<interface>;

        std::vector<ptr_t> _children;

    public:
        add

                interface
        &

        operator[](std::size_t index) {
            return *_children[index];
        }

        static likelihood analyse_mem(void *mem, std::size_t expected_buffer_len = 0) {

        }
    };

}


#endif //HIDENSEEK_STRUCTURE_HPP
